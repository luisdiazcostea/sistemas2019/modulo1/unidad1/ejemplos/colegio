-- Creaci�n de la base de datos
  DROP DATABASE IF EXISTS b20190530;
  CREATE DATABASE IF NOT EXISTS b20190530;
  USE b20190530;

-- Creaci�n de tablas. Total: 6 (3 tablas, 3 relaciones)

 CREATE TABLE profesores (
    dni int AUTO_INCREMENT,
    nombre varchar(100),
    direccion varchar (100),
    telefono varchar(20),
    PRIMARY KEY (dni)
  );

 CREATE TABLE modulos (
    cod int AUTO_INCREMENT,
    nombre varchar(100),
    PRIMARY KEY (cod)
  );

 CREATE TABLE alumnos(
    nexp int AUTO_INCREMENT,
    nombre varchar(50),
    apellidos varchar (100),
    fnac varchar(20),
    PRIMARY KEY (nexp)
  );

CREATE TABLE impartir(
  profdni int,
  modcod int,
  PRIMARY KEY (profdni,modcod),
  UNIQUE KEY (profdni),
  CONSTRAINT fkimpartirprofesor FOREIGN KEY (profdni) REFERENCES profesores (dni),
  CONSTRAINT fkimpartirmodulo FOREIGN KEY (modcod) REFERENCES modulos (cod)
);

CREATE TABLE estudiar(
  alunexp int,
  modcod int,
  PRIMARY KEY (alunexp,modcod),
  CONSTRAINT fkestudiaralumno FOREIGN KEY (alunexp) REFERENCES alumnos (nexp),
  CONSTRAINT fkestudiarmodulo FOREIGN KEY (modcod) REFERENCES modulos (cod)
);

CREATE TABLE representar(
  delnexp int,
  alunexp int,
  PRIMARY KEY (delnexp, alunexp),
  UNIQUE KEY (delnexp),
  CONSTRAINT fkrepresentaralumno FOREIGN KEY (alunexp) REFERENCES alumnos(nexp),
  CONSTRAINT fkreprsentardelegado FOREIGN KEY (delnexp) REFERENCES alumnos(nexp)
);
